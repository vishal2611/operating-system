#include<bits/stdc++.h>
using namespace std;

int main()
{
    int bt[1000],n,ct[1000],tat[1000],wt[1000];
    cout<<"\t\tShortest Job First"<<endl;
    cout<<"Enter the no. of process you want to insert"<<endl;
    cout<<"N=";
    cin>>n;
    
    // suppose here arrival time for all process is Zero...
    // this loop is used to inserts the burst time of N processes
    for(int i=0;i<n;i++)
    {
        cout<<"Burst Time["<<i+1<<"]=";
        cin>>bt[i];
    }
    float t=0,sum=0;
    //sort function for SJF 
    sort(bt,bt+n);
    for(int i=0;i<n;i++)
    {
        t=t+bt[i];
        ct[i]=t;
        // here arrival time for all process is zero so turn arround time is same of completion time
        tat[i]=ct[i]-0;
        wt[i]=tat[i]-bt[i];
        sum+=wt[i];
    }
    cout<<"Arrival\t Burst\tCompletion\tTaT\tWaiting"<<endl;
    for(int i=0;i<n;i++)
    {
        cout<<0<<"\t"<<bt[i]<<"\t"<<ct[i]<<"\t\t"<<tat[i]<<"\t"<<wt[i]<<endl;
    }
    float v=sum/n;
    cout<<"Average waiting time is :"<<v<<endl;
    return 0;
}
